from django.shortcuts import render
from django.utils import timezone
from .models import Post, PostTag

# Create your views here.

def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    favorite_posts = posts[:4]
    for post in posts:
        post.all_tags = post.tags.all()
    return render(request, 'blog/post_list.html', {'posts': posts, 'favorite_posts': favorite_posts})
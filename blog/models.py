from django.conf import settings
from django.db import models
from django.utils import timezone


class Tag(models.Model):
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    comments_count = models.PositiveSmallIntegerField(null=False, default=0)
    img_url = models.CharField(max_length=500)
    post_slug = models.CharField(max_length=200)
    image = models.ImageField(null=True, blank=True, upload_to="media/post_images/")
    tags = models.ManyToManyField(Tag)

    def publish(self, *args, **kwargs):
        self.published_date = timezone.now()
        super().save(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.post_slug = '/posts/' + str(self.title) + "_" + self.created_date.strftime("%Y-%b-%d-%S")
        self.publish(*args, **kwargs)
        

    def __str__(self):
        return self.title

class PostTag(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)